import React,{lazy,Suspense } from 'react'
import {HashRouter, Route, Switch} from 'react-router-dom'
import Login from './pages/Login/login.jsx'
// import Admin from './pages/Admin/admin.jsx'

// const Login = lazy(() => import('./pages/Login/login.jsx'))
const Admin = lazy(() => import('./pages/Admin/admin.jsx'))

function App() {
  return (
       <HashRouter>
        <Switch> {/*只匹配其中一个*/}
          <Route path='/login' component={Login}></Route>
          <Suspense fallback={<div>loading </div>}>  
            <Route path='/' component={Admin}></Route>
          </Suspense> 
        </Switch>
      </HashRouter>
  );
}

export default App;
