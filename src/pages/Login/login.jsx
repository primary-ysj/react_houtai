import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Form, Input, Button, message } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import './login.less'
import { reqLogin } from '../../api'
import { connect } from 'react-redux'

import { receiveUser } from '../../redux/actions'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {}
  //m密码验证函数
  validatePwd = (rule, value, callback) => {
    if (!value) {
      callback('密码必须输入')
    } else if (value.length < 4) {
      callback('密码长度不能小于4位')
    } else if (value.length > 12) {
      callback('密码长度不能大于12位')
    } else if (!/^[a-zA-Z0-9_]+$/.test(value)) {
      callback('密码必须是英文、数字或下划线组成')
    } else {
      callback() // 验证通过
    }
    // callback('xxxx') // 验证失败, 并指定提示的文本
  }
  //验证通过后触发函数
  onFinish = async values => {
    console.log('Received values of form: ', values)
    let { username, password } = values
    const result = await reqLogin(username, password)
    if (result.status === 0) {
      // 登陆成功
      // 提示登陆成功
      message.success('登陆成功')
      // 保存user
      const user = result.data
      console.log(this.props)
      this.props.receiveUser(user)
      // 跳转到管理界面 (不需要再回退回到登陆)
      this.props.history.replace('/home')
    } else {
      // 登陆失败
      // 提示错误信息
      message.error(result.msg)
    }
  }
  render() {
    const user = this.props.user
    if (user && user._id) {
      return <Redirect to="/" />
    }

    return (
      <div className="login">
        <header className="login-header">
          <h1>React项目: 后台管理系统</h1>
        </header>
        <section className="login-content">
          <h2>用户登陆</h2>
          <Form name="normal_login" className="login-form" initialValues={{ remember: true }} onFinish={this.onFinish}>
            <Form.Item
              initialValue="admin"
              name="username"
              rules={[
                { required: true, whitespace: true, message: '用户名必须输入' },
                { min: 4, message: '用户名至少4位' },
                { max: 12, message: '用户名最多12位' },
                { pattern: /^[a-zA-Z0-9_]+$/, message: '用户名必须是英文、数字或下划线组成' },
              ]}
            >
              <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="账户" />
            </Form.Item>
            <Form.Item initialValue="admin" name="password" rules={[{ validator: this.validatePwd }]}>
              <Input prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="密码" />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button">
                登录
              </Button>
            </Form.Item>
          </Form>
        </section>
      </div>
    )
  }
}

export default connect(state => ({ user: state.user }), { receiveUser })(Login)
