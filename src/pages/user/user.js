import React, {Component} from 'react'
import {
  Card,
  Button,
  Table,
  Modal,
  Form,
  Select,
  Input,
  message
} from 'antd'
import {formateDate} from "../../utils/dateUtils"
import LinkButton from "../../components/link-button/index"
import {reqDeleteUser, reqUsers, reqAddOrUpdateUser} from "../../api/index";
const Item = Form.Item
const Option = Select.Option
/*
用户路由
 */
export default class User extends Component {

 

  constructor(props){
    super(props)
    this.state = {
      users: [], // 所有用户列表
      roles: [], // 所有角色列表
      isShow: false, // 是否显示确认框
      user:{}//当前选中的
    }
    this.formRef=React.createRef()
  }
  initColumns = () => {
    this.columns = [
      {
        title: '用户名',
        dataIndex: 'username'
      },
      {
        title: '邮箱',
        dataIndex: 'email'
      },

      {
        title: '电话',
        dataIndex: 'phone'
      },
      {
        title: '注册时间',
        dataIndex: 'create_time',
        render: formateDate
      },
      {
        title: '所属角色',
        dataIndex: 'role_id',
        render: (role_id) => this.roleNames[role_id]
      },
      {
        title: '操作',
        render: (user) => (
          <span>
            <LinkButton onClick={() => this.showUpdate(user)}>修改</LinkButton>
            <LinkButton onClick={() => this.deleteUser(user)}>删除</LinkButton>
          </span>
        )
      },
    ]
  }

  /*
  根据role的数组, 生成包含所有角色名的对象(属性名用角色id值)
   */
  initRoleNames = (roles) => {
    const roleNames = roles.reduce((pre, role) => {
      pre[role._id] = role.name
      return pre
    }, {})
    // 保存
    this.roleNames = roleNames
  }

  /*
  显示添加界面
   */
  showAdd = () => {
    // this.user = null // 去除前面保存的user
    this.setState({isShow: true,user:{}},()=>{
      this.formRef.current.resetFields()

    })
  }

  /*
  显示修改界面
   */
  showUpdate = (user) => {
    // this.user = user // 保存user
    console.log(user);
    this.setState({
      user,
      isShow: true,
    },()=>{
    
      this.formRef.current.resetFields()
    })
  }

  /*
  删除指定用户
   */
  deleteUser = (user) => {
    Modal.confirm({
      title: `确认删除${user.username}吗?`,
      onOk: async () => {
        const result = await reqDeleteUser(user._id)
        if(result.status===0) {
          message.success('删除用户成功!')
          this.getUsers()
        }
      }
    })
  }

  /*
  添加/更新用户
   */
  addOrUpdateUser = async () => {

    
    // 1. 收集输入数据
    const user =await this.formRef.current.validateFields()
    console.log(user);
    if(user){
      this.setState({isShow: false})
      this.formRef.current.resetFields()
      // 如果是更新, 需要给user指定_id属性
      if (this.state.user) {
        user._id = this.state.user._id
      }
  
      // 2. 提交添加的请求
      const result = await reqAddOrUpdateUser(user)
      // 3. 更新列表显示
      if(result.status===0) {
        message.success(`${this.user ? '修改' : '添加'}用户成功`)
        this.getUsers()
      }
    }
  }

  getUsers = async () => {
    const result = await reqUsers()
    if (result.status===0) {
      const {users, roles} = result.data
      this.initRoleNames(roles)
      this.setState({
        users,
        roles
      })
    }
  }

  componentWillMount () {
    this.initColumns()
  }

  componentDidMount () {
    this.getUsers()
  }


  render() {

    const {users, roles, isShow,user} = this.state
    // const user = this.user || {}

    const title = <Button type='primary' onClick={this.showAdd}>创建用户</Button>
    // 指定Item布局的配置对象
    const formItemLayout = {
      labelCol: { span: 4 },  // 左侧label的宽度
      wrapperCol: { span: 15 }, // 右侧包裹的宽度
    }
    return (

      
      <Card title={title}>
        <Table
          bordered
          rowKey='_id'
          dataSource={users}
          columns={this.columns}
          pagination={{defaultPageSize: 2}}
        />

        <Modal
          title={user._id ? '修改用户' : '添加用户'}
          visible={isShow}
          onOk={this.addOrUpdateUser}
          onCancel={() => {
            this.formRef.current.resetFields()
            this.setState({isShow: false})
          }}
        >
          {/* <UserForm
            setForm={form => this.form = form}
            roles={roles}
            user={user}
          /> */}
          <Form ref={this.formRef} {...formItemLayout}>
            <Item rules={[{ required: true, message: '用户名必须输入' }]} initialValue={user.username} name="username" label='用户名'>
              <Input placeholder='请输入用户名'/>
            </Item>
            {
              user._id?null:(
                <Item rules={[{ required: true, message: '密码必须输入' }]} name="password" label='密码'>
                  <Input type='password' placeholder='请输入密码'/>
                </Item>
              )
            }
            <Item rules={[{ required: true, message: '手机号必须输入' }]} initialValue={user.phone}  name="phone" label='手机号'>
              <Input placeholder='请输入手机号'/>
            </Item>
            <Item rules={[{ required: true, message: '邮箱必须输入' }]} initialValue={user.email} name="email" label='邮箱'>
              <Input placeholder='请输入邮箱'/>
            </Item>
            <Item rules={[{ required: true, message: '角色必须选择' }]} initialValue={user.role_id} name="role_id" label='角色'>
              <Select placeholder="请选择角色">
                {
                  roles.map(role => <Option key={role._id} value={role._id}>{role.name}</Option>)
                }
              </Select>
            </Item>
          </Form>
        </Modal>

      </Card>
    )
  }
}