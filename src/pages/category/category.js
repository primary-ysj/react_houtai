import React, {Component} from 'react'
import {
  PlusOutlined,
  MenuFoldOutlined,
  ArrowRightOutlined
} from '@ant-design/icons';
import {
  Card,
  Table,
  Button,
  Select,
  Icon,
  message,
  Modal,
  Tag, 
  Form,
  Input,
  Space 
} from 'antd'
import LinkButton from '../../components/link-button'
import {reqCategorys, reqUpdateCategory, reqAddCategory} from '../../api'
const { Option } = Select;

export default class Category extends Component {

  constructor(props) {
    super(props);
    this.state = {
        loading: false, // 是否正在获取数据中
        category:"",//选中的目标
        categorys: [], // 一级分类列表
        subCategorys: [], // 二级分类列表
        parentId: '0', // 当前需要显示的分类列表的父分类ID
        parentName: '', // 当前需要显示的分类列表的父分类名称
        showStatus: 0, // 标识添加/更新的确认框是否显示, 0: 都不显示, 1: 显示添加, 2: 显示更新
    };
    this.amendRef = React.createRef()
    this.addRef = React.createRef()
  }

  //初始化table
  initColumns=()=>{
      this.columns = [
      {
        title: '分类的名称',
        dataIndex: 'name',
      },
    
      {
        title: '操作',
        width:300,
        render: (category) => ( // 返回需要显示的界面标签
              <span>
                <LinkButton onClick={() => this.showUpdate(category)}>修改分类</LinkButton>
                {/*如何向事件回调函数传递参数: 先定义一个匿名函数, 在函数调用处理的函数并传入数据*/}
                {this.state.parentId==='0' ? <LinkButton onClick={() => this.showSubCategorys(category)}>查看子分类</LinkButton> : null}
        
              </span>
            )
        },
      ];
  }

  //获取一级或二级目录数据
  getCategorys = async (parentId) =>{
    this.setState({loading:true})
    parentId = parentId || this.state.parentId
    //获取数据
    const res = await reqCategorys(parentId)
    console.log(res);
    this.setState({loading:false})
   if(res.status===0) {
      // 取出分类数组(可能是一级也可能二级的)
      const categorys = res.data
      if(parentId==='0') {
        // 更新一级分类状态
        this.setState({
          categorys
        })
        console.log('----', this.state.categorys.length)
      } else {
        // 更新二级分类状态
        this.setState({
          subCategorys: categorys
        })
      }
    } else {
      message.error('获取分类列表失败')
    }

  }

   /*
  显示指定一级分类列表
   */
  showCategorys = () => {
    // 更新为显示一列表的状态
    this.setState({
      parentId: '0',
      parentName: '',
      subCategorys: []
    })
  }
  //点击修改分类
  showUpdate=(category)=>{
     // 保存分类对象
    // 更新状态
    this.setState({
      showStatus: 2,
      category
    },()=>{
      this.amendRef.current.setFieldsValue({amend:category.name})
    })
  }

  
  showSubCategorys=(category)=>{
       // 更新状态
    this.setState({
      parentId: category._id,
      parentName: category.name
    }, () => { // 在状态更新且重新render()后执行
      console.log('parentId', this.state.parentId) // '0'
      // 获取二级分类列表显示
      this.getCategorys()
    })
  }
  //修改的模态框点击确认
  updateCategory=async()=>{
    const fieldsValue =await this.amendRef.current.validateFields(); 
    let categoryId = this.state.category._id
    let {amend} = fieldsValue
    
     const result = await reqUpdateCategory({categoryId, categoryName:amend})
        if (result.status===0) {
          // 3. 重新显示列表
          this.setState({
            showStatus: 0
          },()=>{
            this.amendRef.current.resetFields()
            this.getCategorys()
          })
        }
  }
  //新增分类的弹窗确认
  updateCategory1=async()=>{
    const fieldsValue =await this.addRef.current.validateFields(); 
    console.log(this.addRef.current,fieldsValue);
    let {addType,addName} = fieldsValue
  
     const result = await reqAddCategory(addName,addType)
        if (result.status===0) {
          // 3. 重新显示列表
          this.setState({
            showStatus: 0
          },()=>{
              this.addRef.current.resetFields()
            // 添加的分类就是当前分类列表下的分类
          if(addType===this.state.parentId) {
            // 重新获取当前分类列表显示
            this.getCategorys()
          } else if (addType==='0'){ // 在二级分类列表下添加一级分类, 重新获取一级分类列表, 但不需要显示一级列表
            this.getCategorys('0')
          }
          })
        }
  }
  //隐藏修改弹窗
  handleCancel=()=>{
    // this.amendRef.current.value=""
    console.log(this.amendRef.current);
    this.amendRef.current.resetFields()
    this.setState({
      showStatus: 0
    })
  }
  //隐藏新增弹窗
  handleCancel1=()=>{
     this.addRef.current.resetFields()
    this.setState({
      showStatus: 0
    })
  }
  showAdd=()=>{
    let parentId = this.state.parentId
    this.setState({
        showStatus: 1
    },()=>{this.addRef.current.setFieldsValue({
        addType:parentId
    })})
  }
  componentWillMount() {
    this.initColumns()
  }
  componentDidMount(){
    this.getCategorys()
  }
  
  render() {

   // 读取状态数据
    const {categorys,category, subCategorys, parentId, parentName, loading, showStatus} = this.state
    console.log(parentId);
    // card的左侧
    const title = parentId === '0' ? '一级分类列表' : (
      <span>
        <LinkButton onClick={this.showCategorys}>一级分类列表</LinkButton>
        <ArrowRightOutlined />
        <span>{parentName}</span>
      </span>
    )
      // Card的右侧
    const extra = (
      <Button type='primary' onClick={this.showAdd}>
        <PlusOutlined/>
        添加
      </Button>
    )
    return (
      <div>
         <Card title={title} extra={extra}>
            <Table 
              bordered={true} 
              rowKey='_id'
              loading={loading}
              columns={this.columns} 
              pagination={{defaultPageSize: 5, showQuickJumper: true}}
              dataSource={parentId==='0' ? categorys : subCategorys} />
            <Modal
              title="更新分类"
              cancelText="取消"
              visible={showStatus===2}
              onOk={this.updateCategory}
              onCancel={this.handleCancel}
            >
              <Form name="control-ref"  ref={this.amendRef}>
                <Form.Item  name="amend" rules={[{required: true, message: '分类名称必须输入'}]}>
                  <Input  placeholder="请输入分类名称" />
                </Form.Item>
              </Form>
            </Modal>
            <Modal
              title="添加分类"
              cancelText="取消"
              visible={showStatus===1}
              onOk={this.updateCategory1}
              onCancel={this.handleCancel1}
            >
              <Form name="add-ref"  ref={this.addRef}>
                <Form.Item  initialValue={parentId} name="addType">
                  <Select >
                    <Option value='0'>一级分类</Option>
                    {categorys.map((item)=>{
                      return(
                        <Option key={item._id} value={item._id}>{item.name}</Option>
                      )
                    })}
                  </Select>
                </Form.Item>
                <Form.Item  name="addName" rules={[{required: true, message: '分类名称必须输入'}]}>
                  <Input  placeholder="请输入分类名称" />
                </Form.Item>
              </Form>
            </Modal>
         </Card>
      </div>
    )
  }
}