import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from '@ant-design/icons';
import Home from '../pages/home/home'
import Category from '../pages/category/category'
import Product from '../pages/product/product'
import Role from '../pages/role/role'
import User from '../pages/user/user'
import Bar from '../pages/charts/bar'
import Line from '../pages/charts/line'
import Pie from '../pages/charts/pie'

const menuList = [
  {
    title: '首页', // 菜单标题名称
    key: '/home', // 对应的path
    icon: <MenuUnfoldOutlined />, // 图标名称
    isPublic: true, // 公开的,
    component:Home
  },
  {
    title: '商品',
    key: '/products',
    icon: <MenuUnfoldOutlined />,
    children: [ // 子菜单列表
      {
        title: '品类管理',
        key: '/category',
        icon: <MenuFoldOutlined />,
        component:Category
      },
      {
        title: '商品管理',
        key: '/product',
        icon: <MenuFoldOutlined />,
        component:Product
      },
    ]
  },

  {
    title: '用户管理',
    key: '/user',
    icon: <MenuFoldOutlined />,
    component:User
  },
  {
    title: '角色管理',
    key: '/role',
    icon: <MenuFoldOutlined />,
    component:Role
  },

  {
    title: '图形图表',
    key: '/charts',
    icon: <MenuFoldOutlined />,
    children: [
      {
        title: '柱形图',
        key: '/charts/bar',
        icon: <MenuFoldOutlined />,
        component:Bar
      },
      {
        title: '折线图',
        key: '/charts/line',
        icon: <MenuFoldOutlined />,
        component:Line
      },
      {
        title: '饼图',
        key: '/charts/pie',
        icon: <MenuFoldOutlined />,
        component:Pie
      },
    ]
  },

  // {
  //   title: '订单管理',
  //   key: '/order',
  //   icon: <MenuFoldOutlined />,
  // },
]

export default menuList