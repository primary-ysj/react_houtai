import {
  SET_HEAD_TITLE,
  RECEIVE_USER,
  SHOW_ERROR_MSG,
  RESET_USER
} from './action-types'
import {reqLogin} from '../api'
import storageUtils from "../utils/storageUtils";

/*
设置头部标题的同步action
 */
export const setHeadTitle = (headTitle)=>({type:SET_HEAD_TITLE,data:headTitle})

/*
接收用户的同步action
 */
export const receiveUser = (user) => {
  storageUtils.saveUser(user) // 保存到local中
  return{type: RECEIVE_USER, user}
}

/*
退出登陆的同步action
 */
export const logout = () =>  {
  // 删除local中的user
  storageUtils.removeUser()
  // 返回action对象
  return {type: RESET_USER}
}